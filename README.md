# CLEVERSON BONAMIGO #
E-mail: cleversoncb@gmail.com
Telefone: (45) 99855 1717

### Formação ###

* Pós-graduado em engenharia de software. Conclusão: 2011, Univel.
* Graduado em ciência da computação. Conclusão: 2009, Unipan.

### Experiência profissional ###

* 2009 - Cascavel - Grupo Vasconcelos
Cargo: Apoio de informática/Programador web. 
Principais atividades: Criação de sistemas internos em ambiente web. 
Linguagens/frameworks: Java, php, mysql, HTML, css, javascript. 

* 2010/2012 – Cascavel - Viabr Smart Solutions(grupo viabr)
Cargo: Programador. 
Principais atividades: Desenvolvedor Web Full Stack. 
Linguagens/frameworks: Java, PHP, cakephp, postgresql, mysql, HTML, css, javascript. 

* 2012/2013 – Cascavel - Wealth Systems
Cargo: Desenvolvedor Analista. 
Principais atividades: Desenvolvedor Full Stack web, Windows mobile e android. 
Linguagens/frameworks:  Java, swing(Windows mobile), postgresql, oracle, HTML, css, javascript, vraptor, android. 

* 2014/2015- Florianópolis – Paripassu aplicativos
Cargo: Analista/Programador java.  
Principais atividades: Desenvolvedor Web Full Stack. 
Linguagens/frameworks: Git, Jira, Backbone, angularJS, NodeJS, Java, Spring, JSF, postgresql, HTML5, css3, javascript. 

* 2015/2016/2017- Cascavel – Datacoper Software
Cargo: Analista de sistemas. 
Principais atividades: Arquiteto de front-end. Desenvolvimento e gerenciamento de componentes angularJS
para uso em novo ERP Interpretado e gerado a partir de mapas mentais. 
Linguagens/frameworks:  angularJS, HTML5, css3, javascript, freemind. 

* 2018/2019- Cascavel – Datacoper Software
Cargo: Analista de sistemas. 
Principais atividades: Desenvolvedor Web Full Stack. 
Linguagens/frameworks: Java, angularJS, HTML5, css3, javascript, Hudson, maven, eclipse, web-services REST. 

### Projetos ###

* SisCeleiro+ - Sistema centralizador de estoque Celeiro Nacional.
* LisGo - Sistema de controle de ligações telefônicas.
* SoftBase - Sistema de estoque e vendas para pequenas empresas.
* Cobra Menos - Sistema de compras coletivas.
* Guias comerciais.
* Prefeitura de Tupassi - Aplicações para prefeitura.
* Clube dos formandos - Sistema de administração de formaturas.
* Sitemy - Aplicativo criador de single/one pages.
* Pack Ball - BI de análises estatísticas.
* F5 tickets - Sistema gerenciador de ordens de serviço.
